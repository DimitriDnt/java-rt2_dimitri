package tp4;

// On importe les bibliotheques necessaires

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

public class LaFourmiDeLangton extends JFrame implements ActionListener{
	
	// Attributs
	
	private int dimension;
	private int x;
	private int y;
	private int angle;
	private int nbTours;
	JButton bouton;
	JLabel label1;
	JTextField plateau [][];
	
	// Constructeur
	
	public LaFourmiDeLangton() {
		
		super();
		dimension = 100;
		this.setTitle("La Fourmi de Langton");
		this.setSize(500, 500);
		this.setLocation(300, 200);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fen�tre est cr��e !");
		
		Container fenetre = getContentPane();
		fenetre.setLayout(new BorderLayout());
		bouton = new JButton("Next");
		fenetre.add(bouton, BorderLayout.SOUTH);
		bouton.addActionListener(this);
		label1 = new JLabel("Nombre de tours = 0");
		fenetre.add(label1, BorderLayout.NORTH);
		// Creation du plateau
		fenetre.add(creerPlateau(), BorderLayout.CENTER);
		this.setVisible(true);
	}
	
	// Methodes

	public JPanel creerPlateau() {
		
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(dimension, dimension));
		plateau = new JTextField [dimension][dimension];
		// Creation de la grille, deux boucles FOR imbriquees
		for (int i = 0 ; i < dimension ; i++) {
			
			for (int k = 0 ; k < dimension ; k++) {
				
				JTextField t = new JTextField(); // A chaque case du plateau, on associe un objet JTextField
				plateau [i][k] = t;
				plateau [i][k].setBackground(Color.WHITE); // On definit la couleur des cases sur blanc
				p.add(t);
			}
		}
		return p;
	}
	
	public void initFourmi() {
		x = (dimension/2);
		y = (dimension/2);
		// On place la fourmi le plus pres du centre et on l'initialise � 0 car elle regarde en haut
		plateau[x][y].setText("0");
	}
	
	public void next() {
		// Si la couleur de la case du plateau est noir
		if (plateau[x][y].getBackground() == Color.BLACK) {
			// Elle devient blanche
			plateau[x][y].setBackground(Color.WHITE);
			
			if (angle == 0) {
				
				angle = 90;
				y = y+1;
				nbTours++;
			}
			
			else if (angle == 90) {
				
				angle = 180;
				x = x + 1;
				nbTours++;
			}
			
			else if (angle == 180) {
				
				angle = 270;
				y = y - 1;
				nbTours++;
			}
			
			else if (angle == 270) {
				
				angle = 0;
				x = x - 1;
				nbTours++;
			}	
		}
		// Si la couleur de la case du plateau est blanc
		if (plateau[x][y].getBackground() == Color.WHITE) {
			// Elle devient noire
			plateau[x][y].setBackground(Color.BLACK);
			// Si la fourmi regarde en haut
			if (angle == 0) {
				
				angle = 270; // On modifie la valeur par 270
				y = y - 1; // La fourmi se d�place sur le plateau
				nbTours++; // On augmente le nombre de tours
			}
			// Si la fourmi regarde a droite
			else if (angle == 90) {
				
				angle = 0;
				x = x - 1;
				nbTours++;
			}
			// Si la fourmi regarde en bas
			else if (angle == 180) { 
				
				angle = 90;
				y = y + 1;
				nbTours++;
			}
			// Si la fourmi regarde a gauche
			else if (angle == 270) {
				
				angle = 180;
				x = x + 1;
				nbTours++;
			}
		}
		
		plateau[x][y].setText(" " + angle);
		label1.setText("Nombre de tours : " + nbTours);
	}
	
	// Evenement du clic sur le bouton
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == bouton) {
			
			System.out.println("Click !");
			// Phase chaotique
			for (int c = 0 ; c < 10 ; c++) {
			next(); // On fait appel a la methode next
		}
		}
	}
	
	// Methode main
	
	public static void main(String[] args) {
		
		LaFourmiDeLangton f1 = new LaFourmiDeLangton();
		f1.initFourmi();
	}
}
