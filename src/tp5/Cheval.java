package tp5;

public class Cheval extends Thread implements CoureurHippique {
	
	// Attributs
	
	private int id;
	private int longueurCourse;
	private JugeDeCourse juge;
	private int distance;

	// Constructeur
	
	public Cheval (int i, int l, JugeDeCourse j) {
		
		id = i;
		longueurCourse = l;
		juge = j;
		distance = 0;
	}
	
	// Methodes
	
	public void run() {
		
		while (distance < longueurCourse) {
			
			//distance = distance + 1;
			distance =  distance + (0 + (int) (Math.random() * (3-0) + 1)); // valeur al�atoire entre 0 et 3
			System.out.println("Le cheval " + id + " a parcouru " + distance + " metre(s).");				
			try {
				Thread.sleep(50); // On patiente 50 millisecondes
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}
		
		if (distance == longueurCourse) {
			
			juge.passeLaLigneDArrivee(id);
			System.out.println("Le cheval " + id + " passe la ligne d'arrivee");
		}
	}
	
	public int distanceParcourue() {
		
		return distance;
	}
}
