package tp5;

// On importe les bibliotheques necessaires

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPanel;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

public class Tierce extends JFrame implements ActionListener, JugeDeCourse {
	
	// Attributs
	
	private JButton bouton;
	private JLabel label1;
	Vector <Cheval> listeChevaux = new Vector <Cheval>();
	
	// Constructeur
	
	public Tierce() {

		super();
		this.setTitle("Tierc�");
		this.setSize(350,250);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fen�tre est cree");
		Container fenetre = getContentPane();
		fenetre.setLayout(new BorderLayout());
		bouton = new JButton("GO");
		fenetre.add(bouton, BorderLayout.SOUTH);
		bouton.addActionListener(this);
		label1 = new JLabel("Resultat :");
		fenetre.add(label1, BorderLayout.NORTH);
		// Creation de six chevaux
		for (int i = 1 ; i <= 6 ; i++) {
			
			Cheval cheval = new Cheval(i, 50, this);
			listeChevaux.add(cheval);
		}
		
		this.setVisible(true);
	}
	
	// Methodes
	
	public void actionPerformed (ActionEvent e) {
		
		if (e.getSource() == bouton) {
			
			for (int i = 0; i< 6; i++) {
				listeChevaux.elementAt(i).start();
			}
			System.out.println("PARTEZ !");
		}
	}
	// On ajoute le mot synchronized pour executer tous les Threads
	public synchronized void passeLaLigneDArrivee(int id) {
		
		label1.setText(label1.getText() + " " + id);
	}

	public static void main(String[] args) {
		
		Tierce t1 = new Tierce();
		
	}
}
