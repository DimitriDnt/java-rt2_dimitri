package td1;

public abstract class FormeGeometrique {
	
	// Attributs
	
	Point p;
	
	// Methodes
	
	public void deplacer() {
		
		System.out.println("Je me deplace");
	}
	
	// Methodes abstraites
	
	public abstract float calculerSurface();
	public abstract float calculerPerimetre();
	public abstract void seDecrire();
	
}
