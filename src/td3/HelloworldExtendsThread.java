package td3;

public class HelloworldExtendsThread extends Thread{
	
	// Attributs
	protected String nom;
	
	// Constructeur
	public HelloworldExtendsThread(String nom) {
		
		this.nom = nom;
	}
	
	// Methodes
	public void run() {
		for (int i = 1; i <= 10; i++) {
			try {
				 Thread.sleep(10);
		}
		catch(InterruptedException e) {
			System.err.println(nom + " a �t� interrompu.");
		}
		System.out.println(nom);
		}
	}
	
		public static void main(String[] args) {
		HelloworldExtendsThread hello, world, everybody;
		hello = new HelloworldExtendsThread("Hello");
		world = new HelloworldExtendsThread("World");
		everybody = new HelloworldExtendsThread("Everybody!");
		hello.start();
		world.start();
		everybody.start();
		}
}