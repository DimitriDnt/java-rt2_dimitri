package td3;

public class TestException {

	public static void main(String[] args) {
		
		try {
			MaClasse c1 = new MaClasse(1);
			System.out.println(c1);
		} catch (MonException e) {
			System.out.println(e.getMessage());
		}
	}
}
