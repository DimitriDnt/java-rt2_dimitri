package td3;

public interface Chauffeur {
	
	// Methodes
	
	public abstract void accelerer();
	public abstract void freiner();
	public abstract void tournerADroite();
	public abstract void tournerAGauche();

}
