package td2;

public abstract class Vehicule {
	
	// Attributs
	
	public int id;
	public int compteur;
	public boolean enPanne;
	
	// Methodes
	
	public void avancer() {
		
		System.out.println("J'avance");
	}

	public void afficherNbKm() {
		
		System.out.println("Le vehicule a parcouru " + compteur);
	}
	
	// Methodes abstraites
	
	public abstract void sePresenter();
	
}
