package td2;

public abstract class VehiculeAMoteur extends Vehicule {
	
	// Attributs
	
	public float capacite;
	public float niveau;
	
	// Methodes
	
	public void faireLePlein() {
		
		System.out.println("Je fais le plein");
	}
	
	public void demarrer() {
		
		System.out.println("Je demarre");
	}

}
