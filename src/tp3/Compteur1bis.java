package tp3;

public class Compteur1bis implements Runnable {

	private String nom;

	public Compteur1bis(String nom) {
		this.nom=nom;
	}

	public void run() {
		// Boucle allant de 1 � 1000 qui affiche le num�ro et le nom
		for (int i =1; i< 1000; i++) System.out.print(i + " " + nom );
	}
	
	public static void  main(String args[]){
		Compteur1bis t1, t2, t3;
		
		// Cr�ation des objets compteurs
		t1=new Compteur1bis("Hello ");
		t2=new Compteur1bis("World ");
		t3=new Compteur1bis("and Everybody ");
		
		// Cr�ation des Threads
		Thread thread1 = new Thread(t1);
		Thread thread2 = new Thread(t2);
		Thread thread3 = new Thread(t2);
		
		// Execution des Threads
		thread1.start();
		thread2.start();
		thread3.start();
		
		try {
			Thread.sleep(100); // Pause de 100 ms
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Fin du programme");
		
		System.exit(0); // Termine le programme
	}
	
}
