package tp3;

public class Compte {
	
	// Attribut
	protected double solde;
	
	// Constructeur
	public Compte() {
		
		solde = 0;
	}
	
	// Methodes
	
	public synchronized void verserArgent(double s) {
		
		solde = solde + s;
	}
	
	public void afficherSolde() {
		
		System.out.println("Le solde actuel de votre compte est de " + solde + " euro(s).");
	}
}
