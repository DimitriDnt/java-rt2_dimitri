package tp3;

public class Banque {

	public static void main(String[] args) {
		
		// Exercice 3.2 : Compte, versement, affichage du solde
		
		Compte c1 = new Compte();
		c1.afficherSolde();
		c1.verserArgent(2);
		c1.afficherSolde();
		
		// Exercice 3.3 : Thread
		
		Compte c2 = new Compte();
		VerserPleinArgent v1 = new VerserPleinArgent(c2);
		VerserPleinArgent v2 = new VerserPleinArgent(c2);
		
		Thread thread1 = new Thread(v1);
		Thread thread2 = new Thread(v2);
		thread1.start();
		// On remarque qu'en lan�ant un deuxi�me Thread, le solde n'atteint pas 400
		// Car lorsque t1 a fini d'ajouter l'argent au solde de d�part, il se met en pause et t2 ajoute �galement l'argent au solde
		thread2.start();
	}
}
