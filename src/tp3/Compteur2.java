package tp3;

public class Compteur2 extends Thread {

	private String nom;

	public Compteur2(String nom) {
		this.nom=nom;
	}

	public void run() {
		for (int i =1; i< 1000; i++) System.out.print(i + " " + nom );
	}

	public static void  main(String args[]) {
		Compteur2 t1, t2, t3;
		t1=new Compteur2("Hello ");
		t2=new Compteur2("World ");
		t3=new Compteur2("and Everybody ");

		t1.start();
		t2.start();
		t3.start();

		try {
			t1.join(); // Impose au thread principal de patienter jusqu'� la fin d'ex�cution de t1
			t2.join(); // On ajoute l'appel 
			t3.join(); // On remarque que les 3 Threads ont le temps de s'ex�cuter avant la fin du programme principal
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Fin du programme");
		
		System.exit(0);
	}
}
