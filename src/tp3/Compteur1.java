package tp3;

public class Compteur1 extends Thread {

	private String nom;

	public Compteur1(String nom) {
		this.nom=nom;
	}

	public void run() {
		// Boucle allant de 1 � 1000 qui affiche le num�ro et le nom
		for (int i =1; i< 1000; i++) System.out.print(i + " " + nom );
	}

	public static void  main(String args[]){
		Compteur1 t1, t2, t3;
		
		// Cr�ation des objets compteurs
		t1=new Compteur1("Hello ");
		t2=new Compteur1("World ");
		t3=new Compteur1("and Everybody ");

		t1.start(); // Un thread d�marre
		t2.start();
		t3.start();
		
		try {
			Thread.sleep(100); // Pause de 100 ms qui fait que l'affichage arrive maintenant jusqu'� 1000
		} catch (InterruptedException e) { // L'ordre d'affichage est al�atoire
			
			e.printStackTrace();
		}
		
		
		System.out.println("Fin du programme");
		
		System.exit(0); // Termine le programme
	}
}
