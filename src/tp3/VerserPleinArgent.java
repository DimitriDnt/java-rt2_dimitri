package tp3;

public class VerserPleinArgent implements Runnable {

	// Attribut
	
	protected Compte compte;
	
	// Constructeur
	
	public VerserPleinArgent(Compte c) {
		
		compte = c;
	}
	
	// Methode
	
	public void run() {
		
		for (int i = 1; i <= 100; i++) {
			
			compte.verserArgent(2);
			System.out.println("Solde : " + compte.solde + " euro(s).");
			
			try {
				Thread.sleep(50); // On patiente 50 ms
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}
	}
}
