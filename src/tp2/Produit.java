package tp2;

public abstract class Produit {
	
	// Attributs
	
	protected String nom;
	protected double prixAchat;
	protected double prixVente;
	protected double quantite;
	
	// Constructeur
	
	public Produit(String n, double pA, double pV, double q) {
		
		nom = n;
		prixAchat = pA;
		prixVente = pV;
		quantite = q;
	}
	
	// Accesseurs
	
	public String getNom() {
		
		return nom;
	}
	
	public double getPrixAchat() {
		
		return prixAchat;
	}
	
	public double getPrixVente() {
		
		return prixVente;
	}
	
	public double getQuantite() {
		
		return quantite;
	}

	// Methodes
	
	public void seDecrire() {
		
		System.out.println("Le produit est un(e) " + getNom() + " qui coute " + getPrixVente() + " euro(s).");
		System.out.println("Il y a actuellement " + getQuantite() +  " " + getNom() + " disponible(s).");
		
	}
	
	public void remplirStock(double r) {
		
		quantite = quantite + r;
	}
	
	public void supprimerStock(double s) {
		
		quantite = quantite - s;
	}
}
