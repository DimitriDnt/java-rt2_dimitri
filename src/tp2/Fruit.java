package tp2;

public abstract class Fruit extends Produit {
	
	// Attributs
	
	private String paysOrigine;
	
	// Constructeur
	
	public Fruit(String n, double pA, double pV, double q, String pO) {
		
		super(n, pA, pV, q);
		paysOrigine = pO;
	}
	
	// Accesseurs
	
	public String getPaysOrigine() {
		
		return paysOrigine;
	}
}
