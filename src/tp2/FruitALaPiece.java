package tp2;

public class FruitALaPiece extends Fruit implements VendreALaPiece {
	
	// Constructeur
	
	public FruitALaPiece(String n, double pA, double pV, double q, String pO) {
		
		super(n, pA, pV, q, pO);
	}

	// Methodes
	
	public void vendre(int q, Magasin m) {
		
		if (quantite > q) { // Si la quantit� de produits dans le stock est sup�rieure � la quantit� que l'on veut vendre
			
			quantite = quantite - q; // On enleve la quantit� de produits vendus � la quantit� de d�part
			m.capitalDeDepart = m.capitalDeDepart + prixVente; // On ajoute le prix d'achat au capital du Magasin
			System.out.println(q + " " + getNom() + " ont �t� vendu pour " + prixVente + " euros !");
		}
		
		else {
			
			System.out.println("Vous n'avez plus de " + getNom() + " � vendre !");
		}
	}
}
