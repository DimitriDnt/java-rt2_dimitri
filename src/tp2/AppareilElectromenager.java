package tp2;

public class AppareilElectromenager extends Produit implements VendreALaPiece, Solde {
	
	// Attributs
	
	private int codeBarreFabricant;
	
	// Constructeur
	
	public AppareilElectromenager(String n, double pA, double pV, double q) {
		
		super(n, pA, pV, q);
	}
	
	// Accesseurs
	
	public int getCodeBarre() {
		
		return codeBarreFabricant;
	}

	// Methodes
	
	public void vendre(int q, Magasin m) {
		
		if (quantite > q) { // Si la quantit� de produits dans le stock est sup�rieure � la quantit� que l'on veut vendre
			
			quantite = quantite - q; // On enleve la quantit� de produits vendus � la quantit� de d�part
			m.capitalDeDepart = m.capitalDeDepart + prixVente; // On ajoute le prix d'achat au capital du Magasin
			System.out.println(q + " " + getNom() + " ont �t� vendu pour " + prixVente + " euros !");
		}
		
		else {
			
			System.out.println("Vous n'avez plus de " + getNom() + " � vendre !");
		}
	}
	
	public void debutDesSoldes(double s) {
		
		prixVente = prixVente - (prixVente * (s/100));
		System.out.println("Le produit " + getNom() + " est sold� de " + s + " pourcents.");
	}
	
	public void finDesSoldes (double s) {
		
		prixVente = prixVente + (s/100);
		System.out.println("Les soldes sont termin�es !");
	}
}
