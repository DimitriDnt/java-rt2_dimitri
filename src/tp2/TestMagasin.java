package tp2;

public class TestMagasin {

	public static void main(String[] args) {
		
		// Cr�ation des produits
		
		AppareilElectromenager a1 = new AppareilElectromenager("T�l�vision", 300, 600, 5);
		a1.seDecrire();
		
		FruitEnVrac f1 = new FruitEnVrac("Banane", 5, 5, 10.000, "Amazonie");
		f1.seDecrire();
		
		FruitALaPiece f2 = new FruitALaPiece("Ananas", 5, 5, 10, "Angleterre");
		f2.seDecrire();
		
		// Cr�ation du magasin
		
		Magasin m1 = new Magasin();
		
		// Ajout des produits � la liste du Magasin
		
		m1.ajouterAuStock(a1);
		m1.ajouterAuStock(f1);
		m1.ajouterAuStock(f2);
		
		// Vente des produits 
		
		a1.vendre(2, m1);
		a1.seDecrire();
		
		f1.vendre((float) 5.000, m1);
		f1.seDecrire();
		
		f2.vendre(5, m1);
		f2.seDecrire();
		
		// Solde
		
		a1.debutDesSoldes(20); // On solde le produit Televion de 20%
		a1.vendre(2, m1);
		
		f1.debutDesSoldes(50);
		f1.vendre(2, m1);
		f1.finDesSoldes(100);
		f1.vendre(2, m1);
		
	}
}
