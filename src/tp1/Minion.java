package tp1;

public abstract class Minion {
	
	// Attributs
	
	public int bourse;
	public int dureeCarriere;
	static int numero; 
	
	// Constructeur
	
	public Minion() {
		
		numero++; // Permet de cr�er des Stuarts sans avoir � �crire un num�ro � la cr�ation de l'objet
		bourse = 0;
		dureeCarriere = (6 + (int) (Math.random() * (10-6) + 1)); // Nombre al�atoire entre 6 et 10
	}
}
