package tp1;

public class TestMinion {

	public static void main(String[] args) {
		
		// Exercice 1.1 : Creation et Presentation des Stuarts
		
		Stuart stuart1 = new Stuart();
		stuart1.sePresenter();

		Stuart stuart2 = new Stuart();
		stuart2.sePresenter();
		
		// Exercice 1.2 : Travailler 5 ans
		
		stuart1.sePresenter();
		stuart1.cycleTravail();
		stuart1.sePresenter();
	}
}
