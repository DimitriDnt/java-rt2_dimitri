package tp1;

import java.util.*; // On importe la bibliotheque pour utiliser un vecteur

public class Usine {
	
	// Attributs
	
	private int coffre;
	int c; // Element de la liste
	
	// Cr�ation du vecteur contenant la liste des Stuarts
	
	Vector <Stuart> listeOuvrier = new Vector <Stuart>();
	
	// Methodes
	
	public void recruterStuart(Stuart s) {
		
		listeOuvrier.add(s); // On ajoute un objet de type Stuart � la liste
		System.out.println("Un Stuart rejoint l'Usine !");
	}
	
	public void sonnerLaCloche() {
		
		for (c = 0; c < listeOuvrier.size(); c++) { // Pour tous les �l�ments de la liste
			
			if (listeOuvrier.get(c).dureeCarriere == 0) { // Si la carri�re du Stuart atteint 0
				
				coffre = coffre + (listeOuvrier.get(c).bourse * 20 / 100); // On ajoute 20% de sa bourse au coffre
				listeOuvrier.remove(c); // Il est supprim� de la liste
				System.out.println("Un Stuart a �t� vir� !");
				System.out.println("Le coffre de l'Usine contient " + coffre + " grosses pieces.");
			}
			
			else { 
				
				listeOuvrier.get(c).travailler();
			}
		}	
	}
	
	public void collecterImpot() {
		
		for (c = 0; c < listeOuvrier.size(); c++) {
			
			coffre = coffre + (listeOuvrier.get(c).bourse * 50 / 100); // On ajoute 50% de la bourse du Stuart au coffre
			System.out.println("Le coffre de l'Usine contient " + coffre + " grosses pieces.");
		}
	}
}
