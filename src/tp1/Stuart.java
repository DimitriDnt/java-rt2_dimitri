package tp1;

public class Stuart extends Minion {
	
	// Attributs
	
	private int capaciteDeProduction;
	private int salaire; 
	private int anneeDeTravail; // Ajout de cette variable
	
	// Constructeur
	
	public Stuart () {
		
		super();
		capaciteDeProduction = (15 + (int) (Math.random() * (25-15) + 1)); // Nombre al�atoire entre 15 et 25
		salaire = capaciteDeProduction * (100 + bourse) / 100;
	}
	
	// Methode
	
	public void sePresenter() {
		
		System.out.println("Stuart " + numero + " : " + " J'ai " + bourse + " grosse(s) pi�ce(s) en bourse et " + dureeCarriere + " ann�es avant la retraite." );
	}
	
	public void travailler() {
		
		dureeCarriere = dureeCarriere - 1;
		bourse = bourse + salaire;
		anneeDeTravail = anneeDeTravail + 1;
	}
	
	public void cycleTravail() {
		
		while (anneeDeTravail < 5) {
			travailler();
		}
	}
}
