package tp1;

public class TestUsine {

	public static void main(String[] args) {
		
		// Cr�ation de l'usine
		
		Usine usine1 = new Usine();
		
		// Cr�ation des deux Stuarts
		
		Stuart s1 = new Stuart();
		Stuart s2 = new Stuart();
		s1.sePresenter();
		s2.sePresenter();
		
		// On recrute les deux Stuarts 
		
		usine1.recruterStuart(s1);
		usine1.recruterStuart(s2);
		
		// On fait travailler les Stuarts
		
		usine1.sonnerLaCloche();
		
		while (s1.dureeCarriere > 0) {
			
			usine1.sonnerLaCloche();
		}
		
		s1.sePresenter();
		s2.sePresenter();
		
		usine1.collecterImpot();
		
	}
}
