package tp6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;


public class ChatClient extends JFrame implements ActionListener, Runnable {

	/*___________ Attributs ______________*/
	private JTextField messageAEnvoyer;		//Zone de saisie du message
	private JButton b_Envoyer;				//Le bouton d'envoi du message
	private Socket maSocket;				//Socket du programme client
	private int numeroPort = 8888;			//Port 
	private String adresseServeur = "localhost";//Adresse du serveur
	private PrintWriter writerClient;	//Objet permettant l'�criture de message sur le socket
	private BufferedReader readerClient;
	private JTextArea panneauMessage;
	
	/*___________ Constructeur ______________*/
	public ChatClient(){
		//D�finition de la fen�tre
		super("Client - Panneau d'affichage");
		setSize(300, 300);
		setLocation(300,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Cr�ation des composants graphiques
		messageAEnvoyer = new JTextField(20);
		panneauMessage = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(panneauMessage, 
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		panneauMessage.setEditable(false);
		b_Envoyer = new JButton("Envoyer");
		b_Envoyer.addActionListener(this);

		//Disposition des composants graphiques
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());
		pane.add(messageAEnvoyer, BorderLayout.WEST);
		pane.add(scrollPane, BorderLayout.CENTER);
		pane.add(b_Envoyer, BorderLayout.SOUTH);

		//Cr�ation du Socket client
		try {
			maSocket = new Socket(adresseServeur, numeroPort);
			writerClient = new PrintWriter(maSocket.getOutputStream()); 
			readerClient = new BufferedReader(new InputStreamReader(maSocket.getInputStream()));
		} catch (Exception e) {
			System.out.println("Erreur Cr�ation client");
		}

		//Affichage de la fen�tre
		setVisible(true);
	}

	/*___________ M�thodes ______________*/
	@Override
	public void actionPerformed(ActionEvent e) {
		//Clic du bouton Envoyer d�clanche la m�thode emettre()
		if (e.getSource() == b_Envoyer) {
			emettre();
		}
	}

	public void emettre() {
		//Envoi du message
		try {
			String message = messageAEnvoyer.getText(); 
			writerClient.println(message); //Envoi du texte par l'objet writer
			writerClient.flush();
			// ecouterServeur(); // On ecoute le serveur pour recevoir le message
			messageAEnvoyer.setText("");
			messageAEnvoyer.requestFocus();

		} catch (Exception e) {
			System.out.println("Erreur Envoi du Message");
		}
	}
	
	/*private void ecouterServeur() {

		try {
			System.out.println("Client en attente du message...\n");

			//On lit et on affiche la chaine de caract�re
			String ligne; 
			while ((ligne = readerClient.readLine()) != null){ 
				System.out.println("Message recu : " + ligne + "\n");
			}

		} catch (Exception e) {
			System.out.println("Envoie du message impossible");
		}
	}*/
	
	public void afficherPanneau(String str){
		panneauMessage.append(str);
	}
	
	@Override
	public void run() {
		
		try {
			afficherPanneau("Client en attente du message...\n");

			//On lit et on affiche la chaine de caract�re
			String ligne; 
			while ((ligne = readerClient.readLine()) != null){ 
				afficherPanneau("Message recu : " + ligne + "\n");
			}

		} catch (Exception e) {
			afficherPanneau("Envoie du message impossible");
		}
	}

	/*___________ M�thode Main ______________*/
	public static void main (String[] args){
		ChatClient ch1 = new ChatClient();
		// Creation du thread
		Thread t1 = new Thread(ch1);
		t1.start();
	}
}
